<?php
session_start();

$fw_url = $_SESSION['fw_url'];
$client_id = $_SESSION['client_id'];
$client_secret = $_SESSION['client_secret'];
$redirect_uri= $_SESSION['redirect_uri']; 
$authorization_code = $_GET['code'];
$session_state = $_SESSION['state'];
$state = $_GET['state'];

if(!$_SESSION['access_token']){
    get_access_token();
}

function get_access_token(){
    global $client_id;
    global $client_secret;
    global $redirect_uri;
    global $authorization_code;
    global $fw_url;
    global $session_state;
    global $state;
    
    if(!$authorization_code or ($state == null)){
         return;
    }
    
    $url = "$fw_url/fotoweb/oauth2/token";
    
    $data = array(
        'grant_type' => 'authorization_code',
        'client_id' => $client_id,
        'client_secret' => $client_secret,
        'code' => $authorization_code,
        'redirect_uri' => $redirect_uri
     );
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' =>  http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $result = file_get_contents($url, false, $context);

    $access_token = json_decode($result)->access_token;
    $_SESSION['access_token'] = $access_token;

    $refresh_token = json_decode($result)->refresh_token;
    $_SESSION['refresh_token'] = $refresh_token;

    if ($_SESSION['access_token']){
        echo 'You have now been successfully logged in. Please close this tab and return to your WordPress site.'
        ?>
        <script> window.localStorage.setItem('isLoggedIn', 'true');</script> 
        <?php
    } //Problemet her er at token går ut så da funker det ikke å logge inn
}
?>
