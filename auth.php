<?php
session_start();

$fw_url = $_SESSION['fw_url'];
$client_id = $_SESSION['client_id']; 
$client_secret = $_SESSION['client_secret'];
$redirect_uri= $_SESSION['redirect_uri'];
$authorization_code = $_GET['code'];
$session_state = $_SESSION['state'];
$state = $_GET['state'];

function get_access_token_from_refresh_token(){
    $refresh_token = $_SESSION['refresh_token'];
    global $access_token;
    global $client_id;
    global $client_secret;
    global $fw_url;

    $url = "$fw_url/fotoweb/oauth2/token";

    //Refresh the token
    $data = array(
        'grant_type' => 'refresh_token',
        'refresh_token' => $refresh_token,
        'client_id' => $client_id,
        'client_secret' => $client_secret,
     );
    
    $options = array(
        'http' => array(
            'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
            'method'  => 'POST',
            'content' => http_build_query($data)
        )
    );
    $context  = stream_context_create($options);
    $refresh_result = file_get_contents($url, false, $context);

    if($refresh_result){
        $_SESSION['access_token'] = json_decode($refresh_result)->access_token;
        $_SESSION['refresh_token'] = json_decode($refresh_result)->refresh_token;
    }

    return json_decode($refresh_result)->access_token;
}