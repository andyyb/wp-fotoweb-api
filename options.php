<?php
session_start();

class FotoWeb_Options {

	public $options;
    public $options_name = 'fotoweb_plugin_options';
    public $state;

	public function __construct () {
		add_action('admin_init', array($this, 'register_settings_and_fields'));
        add_action('admin_menu', array($this, 'add_menu_page'));
		$this->options = get_option( $this->options_name);
	}

    public function generateState($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $state = '';
        for ($i = 0; $i < $length; $i++) {
            $state .= $characters[rand(0, $charactersLength - 1)];
        }
        return $state;
    }

    public function add_menu_page() {
		add_menu_page('FotoWare Plugin', 'FotoWare Settings', 'manage_options', 'fotoweb_plugin', array($this, 'display_options_page'), '
        dashicons-admin-generic', 110);
	}

	public function display_options_page () {
		?>
			<div class="wrap">
				<form method="post" action="options.php">
					<?php 
						settings_fields($this->options_name);
						do_settings_sections(__FILE__); 
						submit_button();
					?>
				</form>
			</div>
		<?php
	}
	

	public function register_settings_and_fields () {
		register_setting($this->options_name, $this->options_name, array($this, 'fotoweb_validate_settings')); 
		add_settings_section( 'fw_main_section', 'FotoWare Settings', array($this, 'fw_main_section_cb'), __FILE__ );
		add_settings_field( 'fw_url', 'FotoWare URL', array($this, 'fw_url_setting'), __FILE__, 'fw_main_section');
		add_settings_field( 'client_id', 'Client ID', array($this, 'client_id_setting'), __FILE__, 'fw_main_section');
		add_settings_field( 'client_secret', 'Client Secret', array($this, 'client_secret_setting'), __FILE__, 'fw_main_section');
		add_settings_field( 'redirect_uri', 'Redirect URI', array($this, 'redirect_uri_setting'), __FILE__, 'fw_main_section');
		add_settings_field( 'logout', '', array($this, 'fotoweb_logout_settings'), __FILE__, 'fw_main_section');

	}


	public function fotoweb_validate_settings ($plugin_options) {
		return $plugin_options;
	}


	/*
	*
	* INPUTS
	*
	*/
	public function fw_url_setting () {
		echo "<input name='$this->options_name[fw_url]' type='text' value='{$this->options['fw_url']}' />";
	}
	public function client_id_setting () {
		echo "<input name='$this->options_name[client_id]' type='text' value='{$this->options['client_id']}' />";
	}
	public function client_secret_setting () {
		echo "<input name='$this->options_name[client_secret]' type='text' value='{$this->options['client_secret']}' />";
	}
	public function wordpress_url_setting () {
		echo "<input name='$this->options_name[wordpress_url]' type='text' value='{$this->options['wordpress_url']}' />";
	}

	public function redirect_uri_setting () {
		$fotoware_base_uri = get_site_url();
		echo "<div> $fotoware_base_uri/wp-content/plugins/wp-fotoweb-api/callback.php</div>";
	}
	public function fotoweb_logout_settings(){
			echo '<a href="../wp-content/plugins/wp-fotoweb-api/logout.php" class="button">Log out From FotoWare</a>';
	}
}

